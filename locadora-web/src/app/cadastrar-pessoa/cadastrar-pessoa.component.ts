import { ServiceComponent } from './../service/service.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar-pessoa',
  templateUrl: './cadastrar-pessoa.component.html',
  styleUrls: ['./cadastrar-pessoa.component.css'],
  providers: [MessageService]

})
export class CadastrarPessoaComponent implements OnInit {

  formularioCliente = new FormGroup({
    nome: new FormControl(''),
    cpf: new FormControl(''),
    dtNascimento: new FormControl(''),
    rg: new FormControl(''),
    cnh: new FormControl(''),
    categoria: new FormControl(''),
    dtValidadeCnh: new FormControl(''),
    cep: new FormControl(''),
    logradouro: new FormControl(''),
    bairro: new FormControl(''),
    complemento: new FormControl(''),
    cidade: new FormControl(''),
    uf: new FormControl(''),
    telefone: new FormControl('')
  });

  constructor(private router: Router, private service: ServiceComponent, private messageService: MessageService) { }

  ngOnInit() {

  }

  selecionaimput(id, item) {
    switch (id) {
      case 1:
        this.formularioCliente.value.nome = item.target.value;
        break;

      case 2:
        this.formularioCliente.value.cpf = item.target.value;
        break;

      case 3:
        this.formularioCliente.value.dtNascimento = item.target.value;
        break;

      case 4:
        this.formularioCliente.value.rg = item.target.value;
        break;

      case 5:
        this.formularioCliente.value.cnh = item.target.value;
        break;

      case 6:
        this.formularioCliente.value.categoria = item.target.value;
        break;

      case 7:
        this.formularioCliente.value.dtValidadeCnh = item.target.value;
        break;

      case 8:
        this.formularioCliente.value.cep = item.target.value;
        break;

      case 9:
        this.formularioCliente.value.logradouro = item.target.value;
        break;

      case 10:
        this.formularioCliente.value.bairro = item.target.value;
        break;

      case 11:
        this.formularioCliente.value.complemento = item.target.value;
        break;

      case 12:
        this.formularioCliente.value.cidade = item.target.value;
        break;

      case 13:
        this.formularioCliente.value.uf = item.target.value;
        break;

      case 14:
        this.formularioCliente.value.telefone = item.target.value;
        break;

      default:
        break;
    }
  }

  salvar() {

    this.service.consultarPessoaCpf(this.formularioCliente.value.cpf)
      .subscribe(data => {
        if (data == null) {
          this.service.salvar(this.formularioCliente.value)
            .subscribe(results => {
              this.messageService.add({ severity: 'success', summary: 'success Message', detail: 'Cliente cadastrado com sucesso' });
            });

          setTimeout(() => {
            this.router.navigate(['/alugar']);
          }, 3000);

        } else {
          this.messageService.add({ severity: 'warn', summary: 'Warn Message', detail: 'CPF já Cadastrado' });
        }
      })
  }

}
