import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceComponent } from '../service/service.component';
import { MessageService } from 'primeng/api';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-consultar-pessoa',
  templateUrl: './consultar-pessoa.component.html',
  styleUrls: ['./consultar-pessoa.component.css'],
  providers: [MessageService]
})
export class ConsultarPessoaComponent implements OnInit {
  formularioCliente = new FormGroup({
    nome: new FormControl(''),
    cpf: new FormControl(''),
    dtNascimento: new FormControl(''),
    rg: new FormControl(''),
    cnh: new FormControl(''),
    categoria: new FormControl(''),
    dtValidadeCnh: new FormControl(''),
    cep: new FormControl(''),
    logradouro: new FormControl(''),
    bairro: new FormControl(''),
    complemento: new FormControl(''),
    cidade: new FormControl(''),
    uf: new FormControl(''),
    telefone: new FormControl('')
  });
  objetoFormulario: any;
  mostraFormulario = false;

  constructor(private router: Router, private service: ServiceComponent, private messageService: MessageService) { }

  ngOnInit(): void {
    this.mostraFormulario = false;
  }

  clienteCpf(cpf) {
    this.formularioCliente.value.cpf = cpf.target.value;
  }

  consultarCliente() {
    this.service.consultarAluguel(this.formularioCliente.value.cpf)
      .subscribe(data => {
        this.objetoFormulario = data;

        this.formularioCliente = this.objetoFormulario;

        if (this.formularioCliente != null) {
          this.mostraFormulario = true;
        }

      })
  }

  pdf(formulario) {
    this.service.geraPdf(formulario)
    .subscribe(res => {

    })
    setTimeout(() => {
      window.open('http://localhost:2020/locadora/pdfAlugar.pdf', '_blank');
    }, 2000);
  }

}
