import { RouterModule, Routes } from '@angular/router';
import { ConsultarPessoaComponent } from './consultar-pessoa/consultar-pessoa.component';
import { EditarPessoaComponent } from './editar-pessoa/editar-pessoa.component';
import { CadastrarPessoaComponent } from './cadastrar-pessoa/cadastrar-pessoa.component';
import { AlugarComponent } from './alugar/alugar.component';
import { NgModule, ModuleWithProviders } from '@angular/core';


const routes: Routes = [
    { path: '', component: AlugarComponent },
    { path: 'alugar', component: AlugarComponent },
    { path: 'cadastrar', component: CadastrarPessoaComponent },
    { path: 'editar', component: EditarPessoaComponent },
    { path: 'consultar', component: ConsultarPessoaComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
