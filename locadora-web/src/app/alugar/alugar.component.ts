import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceComponent } from '../service/service.component';
import { MessageService } from 'primeng/api';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-alugar',
  templateUrl: './alugar.component.html',
  styleUrls: ['./alugar.component.css'],
  providers: [MessageService]
})
export class AlugarComponent implements OnInit {

  formularioCliente = new FormGroup({
    cpf: new FormControl(''),
    veiculo: new FormControl(''),
    placa: new FormControl(''),
    quantidadeDiaria: new FormControl(''),
    valorDiaria: new FormControl(''),
    totalDiaria: new FormControl(''),
    dtLocacao: new FormControl(''),
    dtDevolucao: new FormControl(''),
    horaLocacao: new FormControl(''),
    horaDevolucao: new FormControl('')
  });
  objetoFormulario: any;
  mostraFormulario = false;
  total: any;

  constructor(private router: Router, private service: ServiceComponent, private messageService: MessageService) { }

  ngOnInit(): void {
    this.mostraFormulario = false;
  }

  consultarCliente() {
    this.service.consultarPessoaCpf(this.formularioCliente.value.cpf)
      .subscribe(data => {
        this.objetoFormulario = data;

        if (this.objetoFormulario.length != 0) {
          this.mostraFormulario = true;
        }

      })
  }

  selecionaimput(id, item) {
    switch (id) {
      case 1:
        this.formularioCliente.value.veiculo = item.target.value;
        break;

      case 2:
        this.formularioCliente.value.placa = item.target.value;
        break;

      case 3:
        this.formularioCliente.value.quantidadeDiaria = item.target.value;
        break;

      case 4:
        this.formularioCliente.value.valorDiaria = item.target.value;
        break;

      case 5:
        this.formularioCliente.value.totalDiaria = item.target.value;
        break;

      case 6:
        this.formularioCliente.value.dtLocacao = item.target.value;
        break;

      case 7:
        this.formularioCliente.value.dtDevolucao = item.target.value;
        break;

      case 8:
        this.formularioCliente.value.horaLocacao = item.target.value;
        break;

      case 9:
        this.formularioCliente.value.horaDevolucao = item.target.value;
        break;

      default:
        break;
    }

    this.total = this.formularioCliente.value.quantidadeDiaria * this.formularioCliente.value.valorDiaria;
    this.formularioCliente.value.totalDiaria = this.total;

  }

  salvar() {
    this.service.alugar(this.formularioCliente.value)
      .subscribe(data => {
        console.log(data)
        this.service.geraPdf(data)
          .subscribe(res => {

          })
      })

    setTimeout(() => {
      window.open('http://localhost:2020/locadora/pdfAlugar.pdf', '_blank');
      this.router.navigate(['/cadastrar']);
    }, 2000);


  }

  clienteCpf(cpf) {
    this.formularioCliente.value.cpf = cpf.target.value;
  }

}
