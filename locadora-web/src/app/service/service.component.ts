import { environment } from './../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  salvar(formulario) {
    return this.http.post("/api/locadora/cadastrarPessoa", formulario);
  }

  alugar(formularioAlugar) {
    return this.http.post("/api/locadora/cadastrarAluguel", formularioAlugar);
  }

  geraPdf(formularioAlugar) {
    return this.http.post("/api/locadora/geraPdf", formularioAlugar);
  }

  consultarAluguel(cpf) {
    return this.http.get("/api/locadora/Alugar/" + cpf);
  }

  consultarPessoaCpf(cpf) {
    return this.http.get("/api/locadora/consultarCliente/" + cpf);
  }

}
