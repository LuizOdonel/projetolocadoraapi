import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ServiceComponent } from '../service/service.component';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-pessoa',
  templateUrl: './editar-pessoa.component.html',
  styleUrls: ['./editar-pessoa.component.css'],
  providers: [MessageService]

})
export class EditarPessoaComponent implements OnInit {
  formularioCliente = new FormGroup({
    id: new FormControl(''),
    nome: new FormControl(''),
    cpf: new FormControl(''),
    dtNascimento: new FormControl(''),
    rg: new FormControl(''),
    cnh: new FormControl(''),
    categoria: new FormControl(''),
    dtValidadeCnh: new FormControl(''),
    cep: new FormControl(''),
    logradouro: new FormControl(''),
    bairro: new FormControl(''),
    complemento: new FormControl(''),
    cidade: new FormControl(''),
    uf: new FormControl(''),
    telefone: new FormControl('')
  });

  objetoFormulario: any;
  mostraFormulario = false;

  constructor(private router: Router, private service: ServiceComponent, private messageService: MessageService) { }

  ngOnInit() {
    this.mostraFormulario = false;
  }

  selecionaimput(id, item) {
    switch (id) {
      case 1:
        this.objetoFormulario.nome = item.target.value;
        break;

      case 2:
        this.objetoFormulario.cpf = item.target.value;
        break;

      case 3:
        this.objetoFormulario.dtNascimento = item.target.value;
        break;

      case 4:
        this.objetoFormulario.rg = item.target.value;
        break;

      case 5:
        this.objetoFormulario.cnh = item.target.value;
        break;

      case 6:
        this.objetoFormulario.categoria = item.target.value;
        break;

      case 7:
        this.objetoFormulario.dtValidadeCnh = item.target.value;
        break;

      case 8:
        this.objetoFormulario.cep = item.target.value;
        break;

      case 9:
        this.objetoFormulario.logradouro = item.target.value;
        break;

      case 10:
        this.objetoFormulario.bairro = item.target.value;
        break;

      case 11:
        this.objetoFormulario.complemento = item.target.value;
        break;

      case 12:
        this.objetoFormulario.cidade = item.target.value;
        break;

      case 13:
        this.objetoFormulario.uf = item.target.value;
        break;

      case 14:
        this.objetoFormulario.telefone = item.target.value;
        break;

      default:
        break;
    }
  }

  salvar() {
    console.log(this.formularioCliente)
    this.service.salvar(this.formularioCliente)
      .subscribe(results => {
        this.messageService.add({ severity: 'success', summary: 'success Message', detail: 'Cliente alterado com sucesso' });

        setTimeout(() => {
          this.router.navigate(['/alugar']);
        }, 3000);

      });
  }

  clienteCpf(cpf) {
    this.formularioCliente.value.cpf = cpf.target.value;
  }

  consultarCliente() {
    this.service.consultarPessoaCpf(this.formularioCliente.value.cpf)
      .subscribe(data => {
        this.objetoFormulario = data;

        this.formularioCliente = this.objetoFormulario;

        if (this.formularioCliente != null) {
          this.mostraFormulario = true;
        }

      })
  }

}
