package com.locadora.crud.example.controller;

import com.locadora.crud.example.entity.Alugar;
import com.locadora.crud.example.entity.Pessoa;
import com.locadora.crud.example.service.LocadoraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LocadoraController {
  @Autowired
  private LocadoraService service;
  
  @PostMapping({"/cadastrarPessoa"})
  public Pessoa addPessoa(@RequestBody Pessoa pessoa) {
    return this.service.savePessoa(pessoa);
  }
  
  @PostMapping({"/cadastrarAluguel"})
  public Alugar addPessoa(@RequestBody Alugar alugar) {
    return this.service.saveAlugar(alugar);
  }
  
  @GetMapping({"/Pessoas"})
  public List<Pessoa> findAllPessoas() {
    return this.service.getPessoas();
  }
  
  @GetMapping({"/testes"})
  public String all() {
    return "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh";
  }
  
  @GetMapping({"/Alugar/{cpf}"})
  public List<Alugar> pdf(@PathVariable String cpf) {
    return this.service.getAlugar(cpf);
  }
  
  @PostMapping({"/geraPdf"})
  public String findAllAlugar(@RequestBody Alugar alugar) {
    return this.service.gerarPdf(alugar);
  }
  
  @GetMapping({"/PessoasLista/{cpf}"})
  public List<Pessoa> pesquisaListaPessoaCpf(@PathVariable String cpf) {
    return this.service.getPessoas(cpf);
  }
  
  @GetMapping({"/consultarCliente/{cpf}"})
  public Pessoa pesquisarPessoaCpf(@PathVariable String cpf) {
    return this.service.getPessoaByCpf(cpf);
  }
  
  @DeleteMapping({"/delete/{id}"})
  public String deletePessoa(@PathVariable int id) {
    return this.service.deletePessoa(id);
  }
}