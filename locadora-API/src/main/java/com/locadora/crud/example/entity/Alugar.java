package com.locadora.crud.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ALUGAR")
public class Alugar {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  
  private String cpf;
  
  private String veiculo;
  
  private String placa;
  
  private int quantidadeDiaria;
  
  private double valorDiaria;
  
  private double totalDiaria;
  
  private String dtLocacao;
  
  private String dtDevolucao;
  
  private String horaLocacao;
  
  private String horaDevolucao;
  
  public int getId() {
    return this.id;
  }
  
  public void setId(int id) {
    this.id = id;
  }
  
  public String getCpf() {
    return this.cpf;
  }
  
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
  
  public String getVeiculo() {
    return this.veiculo;
  }
  
  public void setVeiculo(String veiculo) {
    this.veiculo = veiculo;
  }
  
  public String getPlaca() {
    return this.placa;
  }
  
  public void setPlaca(String placa) {
    this.placa = placa;
  }
  
  public int getQuantidadeDiaria() {
    return this.quantidadeDiaria;
  }
  
  public void setQuantidadeDiaria(int quantidadeDiaria) {
    this.quantidadeDiaria = quantidadeDiaria;
  }
  
  public double getValorDiaria() {
    return this.valorDiaria;
  }
  
  public void setValorDiaria(double valorDiaria) {
    this.valorDiaria = valorDiaria;
  }
  
  public double getTotalDiaria() {
    return this.totalDiaria;
  }
  
  public void setTotalDiaria(double totalDiaria) {
    this.totalDiaria = totalDiaria;
  }
  
  public String getDtLocacao() {
    return this.dtLocacao;
  }
  
  public void setDtLocacao(String dtLocacao) {
    this.dtLocacao = dtLocacao;
  }
  
  public String getHoraLocacao() {
    return this.horaLocacao;
  }
  
  public void setHoraLocacao(String horaLocacao) {
    this.horaLocacao = horaLocacao;
  }
  
  public String getHoraDevolucao() {
    return this.horaDevolucao;
  }
  
  public void setHoraDevolucao(String horaDevolucao) {
    this.horaDevolucao = horaDevolucao;
  }
  
  public String getDtDevolucao() {
    return this.dtDevolucao;
  }
  
  public void setDtDevolucao(String dtDevolucao) {
    this.dtDevolucao = dtDevolucao;
  }
}

