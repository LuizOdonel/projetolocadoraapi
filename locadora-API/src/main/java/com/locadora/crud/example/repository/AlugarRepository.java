package com.locadora.crud.example.repository;


import com.locadora.crud.example.entity.Alugar;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlugarRepository extends JpaRepository<Alugar, Integer> {
  Alugar findByCpf(String paramString);
  
  List<Alugar> findAllByCpf(String paramString);
}