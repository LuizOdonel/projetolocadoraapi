package com.locadora.crud.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PESSOA")
public class Pessoa {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  
  private String nome;
  
  private String cpf;
  
  private String dtNascimento;
  
  private String rg;
  
  private String cnh;
  
  private String categoria;
  
  private String dtValidadeCnh;
  
  private String cep;
  
  private String logradouro;
  
  private String bairro;
  
  private String complemento;
  
  private String cidade;
  
  private String uf;
  
  private String telefone;
  
  
  public int getId() {
    return this.id;
  }
  
  public String getRg() {
	return rg;
}

public void setRg(String rg) {
	this.rg = rg;
}

public void setId(int id) {
    this.id = id;
  }
  

  public String getCnh() {
	return cnh;
}

public void setCnh(String cnh) {
	this.cnh = cnh;
}

public String getNome() {
    return this.nome;
  }
  
  public void setNome(String nome) {
    this.nome = nome;
  }
  
  public String getCpf() {
    return this.cpf;
  }
  
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
  
  public String getDtNascimento() {
    return this.dtNascimento;
  }
  
  public void setDtNascimento(String dtNascimento) {
    this.dtNascimento = dtNascimento;
  }
  
  public String getCategoria() {
    return this.categoria;
  }
  
  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }
  
  public String getDtValidadeCnh() {
    return this.dtValidadeCnh;
  }
  
  public void setDtValidadeCnh(String dtValidadeCnh) {
    this.dtValidadeCnh = dtValidadeCnh;
  }
  
  public String getCep() {
    return this.cep;
  }
  
  public void setCep(String cep) {
    this.cep = cep;
  }
  
  public String getLogradouro() {
    return this.logradouro;
  }
  
  public void setLogradouro(String logradouro) {
    this.logradouro = logradouro;
  }
  
  public String getBairro() {
    return this.bairro;
  }
  
  public void setBairro(String bairro) {
    this.bairro = bairro;
  }
  
  public String getComplemento() {
    return this.complemento;
  }
  
  public void setComplemento(String complemento) {
    this.complemento = complemento;
  }
  
  public String getCidade() {
    return this.cidade;
  }
  
  public void setCidade(String cidade) {
    this.cidade = cidade;
  }
  
  public String getUf() {
    return this.uf;
  }
  
  public void setUf(String uf) {
    this.uf = uf;
  }
  
  public String getTelefone() {
    return this.telefone;
  }
  
  public void setTelefone(String telefone) {
    this.telefone = telefone;
  }
}

