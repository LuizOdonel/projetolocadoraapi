package com.locadora.crud.example.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.locadora.crud.example.entity.Alugar;
import com.locadora.crud.example.entity.Pessoa;
import com.locadora.crud.example.repository.AlugarRepository;
import com.locadora.crud.example.repository.PessoaRepository;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocadoraService {
	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private AlugarRepository alugarRepository;

	public Pessoa savePessoa(Pessoa pessoa) {
		return (Pessoa) this.pessoaRepository.save(pessoa);
	}

	public List<Pessoa> savePessoas(List<Pessoa> pessoas) {
		return this.pessoaRepository.saveAll(pessoas);
	}

	public List<Pessoa> getPessoas() {
		return this.pessoaRepository.findAll();
	}

	public List<Alugar> getAlugar(String cpf) {
		return this.alugarRepository.findAllByCpf(cpf);
	}

	public List<Pessoa> getPessoas(String cpf) {
		return this.pessoaRepository.findAllByCpf(cpf);
	}

	public Pessoa getPessoaById(int id) {
		return this.pessoaRepository.findById(Integer.valueOf(id)).orElse(null);
	}

	public Pessoa getPessoaByCpf(String cpf) {
		return this.pessoaRepository.findByCpf(cpf);
	}

	public String deletePessoa(int id) {
		this.pessoaRepository.deleteById(Integer.valueOf(id));
		return "Pessoa removed !! " + id;
	}

	public Alugar saveAlugar(Alugar alugar) {
		return this.alugarRepository.save(alugar);
	}

	public static final Font BOLD_UNDERLINED = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD | Font.UNDERLINE);
	public static final Font NORMAL = new Font(FontFamily.TIMES_ROMAN, 12);

	public String gerarPdf(Alugar alugar) {
		File pdf = new File("C:\\Program Files\\Apache Software Foundation\\Tomcat 10.0\\webapps\\locadora\\pdf.pdf");
		File pdfConsulta = new File(
				"C:\\Program Files\\Apache Software Foundation\\Tomcat 10.0\\webapps\\locadora\\pdfAlugar.pdf");
		Document document = new Document(PageSize.A4);
		try {
			Pessoa pessoa = this.pessoaRepository.findByCpf(alugar.getCpf());
			PdfWriter.getInstance(document, new FileOutputStream(pdf.getAbsolutePath()));
			PdfWriter.getInstance(document, new FileOutputStream(pdfConsulta.getAbsolutePath()));
			if (document.isOpen() == false) {
				document.open();
			}
			String filename = "C:\\locadora\\logoLocadora.png";
			Image image = Image.getInstance(filename);
			document.add((Element) image);
			PdfWriter.getInstance(document, new FileOutputStream(pdf));
			PdfWriter.getInstance(document, new FileOutputStream(pdfConsulta));

			Paragraph titulo = new Paragraph();
			titulo.add(new Phrase("INSTRUMENTO PARTICULAR DE CONTRATO DE LOCACAO DE VEICULOS ", BOLD_UNDERLINED));
			titulo.setAlignment(Element.ALIGN_CENTER);
			titulo.setIndentationLeft(18);
			titulo.setFirstLineIndent(-18);
			titulo.add("\r\n");
			titulo.add("\r\n");

			Paragraph p = new Paragraph();
			p.add("      Pelo presente Instrumento Particular de Contrato de Locade Vee na melhor forma de direito, de um lado,");
			p.add(new Phrase(" AMANDA LOCADORA DE VERENT A CAR ", BOLD_UNDERLINED));
			p.setAlignment(Element.ALIGN_JUSTIFIED);
			p.setIndentationLeft(18);
			p.setFirstLineIndent(-18);
			p.add("com sede a Avenida São José Tocantins, n° 94, bairro Bela Vista no Estado de Goiás, neste ato devidamente representada de acordo com seus Estatutos Sociais, com CNPJ sob o n° ");
			p.add(new Phrase("33.804.666/0001-51 ", BOLD_UNDERLINED));
			p.add(", doravante denominada LOCADORA, e de outro lado ");
			p.add(new Phrase(pessoa.getNome().toUpperCase(), BOLD_UNDERLINED));
			p.add(" residente e domiciliado ");
			p.add(new Phrase(pessoa.getLogradouro() + " " + pessoa.getComplemento() + " " + pessoa.getBairro() + " "
					+ pessoa.getCidade(), BOLD_UNDERLINED));
			p.add(", conforme documentos apresentados no ato da celebração contratual, regularmente inscrito(a) no RG o n° ");
			p.add(new Phrase(pessoa.getRg().toUpperCase(), BOLD_UNDERLINED));
			p.add(" e CPF n° ");
			p.add(new Phrase(pessoa.getCpf().toUpperCase(), BOLD_UNDERLINED));
			p.add(" e CNH n° ");
			p.add(new Phrase(pessoa.getCnh().toUpperCase(), BOLD_UNDERLINED));
			p.add(", doravante denominado(a) LOCATARIO(A), tem entre si, certo e ajustado o presente, o qual se regera pelas cláusulas e condições que a seguir se aduzem com inteira submissão as disposições legais e regulamentares atinentes a espécie. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA PRIMEIRA - DO OBJETO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("1.1- O presente contrato tem por objeto a locação de um veículo, conforme discriminação abaixo: Local retirada/veículo: Amanda Locadora Rent a Car Data: "
					+ alugar.getDtLocacao() + "  hrs: " + alugar.getHoraLocacao()
					+ " Local devolução/veículo: Amanda Locadora Rent a Car Data: " + alugar.getDtDevolucao() + " hrs: "
					+ alugar.getHoraDevolucao() + " Veículo locado: " + alugar.getVeiculo() + " Placa: "
					+ alugar.getPlaca() + " Valor da diária: R$ " + alugar.getTotalDiaria() + "\r\n"
					+ "1.2 - O LOCATARIO poderá, a qualquer momento, solicitar através de correspondência a LOCADORA, redução ou aumento na quantidade de veículos locados, e neste caso, a LOCADORA deverá atender no prazo máximo de 30 (trinta) dias.\r\n"
					+ "1.3 - O LOCATARIO poderá solicitar, a qualquer momento, a inclusão de veículos de marcas e tipos diferentes ao citado na Clausula 1.1 deste Contrato, desde que os preços sejam previamente acordados com a LOCADORAe haja disponibilidade do(s) veículos na sede da LOCADORA. ");
			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA SEGUNDA - DAS OBRIGAÇÕES DA LOCADORA: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("2.1 - Arcar com o pagamento das despesas com os veículos que locar no âmbito deste Contrato, tais como: impostos federais, estaduais, municipais ou quaisquer outras despesas que sejam devidas em decorrência deste contrato.\r\n"
					+ "2.2 - Substituir qualquer veículo que se torne inoperante por motivo de abalroamento, incêndio ou roubo, por outro do mesmo modelo ou similar, desde que haja comprovado culpa exclusiva do LOCATÁRIO, o que será devido arcar com todas as custas de reparo ou compra de um novo equipamento compatível com o locado. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLÁUSULA TERCEIRA - DAS OBRIGAÇÕES DO LOCATÁRIO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("3.1 - Permitir a LOCADORA, na pessoa de seus empregados devidamente "
					+ "credenciados, o livre acesso as suas dependências, visando ao "
					+ "atendimento e a perfeita execução dos serviços objeto deste Contrato.\r\n"
					+ "3.2 - O LOCATARIO obriga-se a não ceder, emprestar ou sublocar o veículo "
					+ "locado, sob pena de ter o presente Contrato imediatamente rescindido, "
					+ "perdendo a validade e a forma de proteção contratada, e ainda, vencendose antecipadamente a dívida.\r\n"
					+ "3.2.1 - A inobservância da Clausula 3.2 do presente Contrato, acarretara "
					+ "ainda a imediata devolução do veículo locado e a sua retenção ensejara a "
					+ "LOCADORA a propositura das medidas judiciais cabíveis a sua retomada "
					+ "forcada, inclusive as criminais.\r\n"
					+ "3.3 - Arcar com o pagamento de todas as multas e penalidades decorrentes "
					+ "de infrações as leis e regulamentos de trânsito, durante o período em "
					+ "que estiver de posse do veículo, salvo se tais multas ou penalidades "
					+ "forem imputáveis a LOCADORA, por irregularidade na documentação do " + "veículo.\r\n"
					+ "3.4 - Providenciar Boletim de Ocorrência Policial, em casos de "
					+ "acidentes, incêndio ou roubo do veículo, e encaminhar imediatamente a "
					+ "LOCADORA para que esta possa tomar as providências necessárias quanto a "
					+ "Proteção contratada.\r\n"
					+ "3.5 - Na falta do Boletim de Ocorrência Policial, citado na Cláusula 3.4 "
					+ "deste Contrato, o LOCATARIO arcará com todas as despesas decorrentes de "
					+ "acidentes, roubos ou incêndios.\r\n"
					+ "3.6 - Arcar com as despesas de combustível, conserto de pneus e câmaras "
					+ "de ar, e substituição de acessórios danificados e/ou não entregues no "
					+ "ato da devolução do veículo.\r\n"
					+ "3.7 - Executar e arcar com os custos de toda a manutenção preventiva e "
					+ "corretiva de acordo com as especificações do fabricante e sempre que "
					+ "houver dúvidas, entrar em contato com a LOCADORA, para mais informações "
					+ "acerca do veículo.\r\n"
					+ "3.8 – Fica acordado entre as partes que será devido no ato da devolução"
					+ "o pagamento da “lavada”, caso a vistoria constate divergência do estado"
					+ "em que o veículo foi entregue. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA QUARTA - DO ACOMPANHAMENTO E FISCALIZACAO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("4.1 - O acompanhamento e a fiscalização a ser exercida pelo LOCATARIO,"
					+ "através de seus prepostos, consistira no direito de vistoriar os"
					+ "veículos quando do recebimento e durante o período de permanência dos "
					+ "mesmos em posse do LOCÁTARIO.\r\n"
					+ "4.2 - A LOCADORA poderá acompanhar e fiscalizar a forma de utilização "
					+ "dos veículos locados, e orientar o LOCATARIO no sentido de atender as "
					+ "especificações recomendadas pelas montadoras.\r\n"
					+ "4.2.1 - O LOCATARIO arcara com todos os danos causados aos veículos "
					+ "quando utilizados em condições anormais, isto e, fora das especificações "
					+ "recomendadas pelas montadoras. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA QUINTA - DO PRAZO DE LOCACAO E VIGENCIA: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("5.1 - O presente Contrato, vigorará com início em " + alugar.getDtLocacao() + " e término " + "em "
					+ alugar.getDtDevolucao() + "  podendo ser prorrogado, por acordo entre as partes "
					+ "mediante assinatura de termo aditivo ao contrato, atendendo as "
					+ "disposições regulamentares. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA SEXTA - DO PREÇO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("6.1.1 – No valor da locação estão inclusos: Franquia de Km/Mês/Veiculo livre; Substituição do veículo em casos de colisão, roubo, furto e incêndio; Taxa de 5% sobre o valor mensal.\r\n"
					+ "6.1.2 – No preço convencionado não está incluso: Motorista; Combustível; Manutenção preventiva e corretiva. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA SETIMA - DO REAJUSTE: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("7.1 - O preço mensal será reajustado anualmente, todo dia 1 (primeiro) do primeiro mês do ano, conforme o índice de reajuste do mercado padrão – INPC/IBGE. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA OITAVA - DO FATURAMENTO E PAGAMENTO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("8.1 – O pagamento da LOCAÇÃO é imediato ao ato de assinatura do "
					+ "contrato, com vencimento mensalmente até o 5º (quinto) dia útil "
					+ "subsequente a data de abertura do Contrato.\r\n"
					+ "8.2 - O pagamento deverá ser efetuado até o 30º (trigésimo) dia "
					+ "subsequente a data de abertura do Contrato.\r\n"
					+ "8.3 - Na ocorrência de atrasos no pagamento, deverá ser pago a LOCADORA,\r\n"
					+ "o valor da locação acrescido de multa moratória, a razão de 10% (dez por "
					+ "cento), juros de 12% (doze por cento) ao ano e correção monetária combase em índices determinados pelo Governo Federal. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA NONA - DA CONFIDENCIALIDADE: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("9.1 - Quaisquer dados e informações, seja qual for a espécie ou "
					+ "natureza, que a LOCADORA através de seus funcionários ou prepostos "
					+ "tenham acesso em decorrência do presente Contrato serão tratados pela "
					+ "LOCADORA como estritamente confidenciais no sentido de que o seu "
					+ "conteúdo, total ou parcial, não seja, em hipótese alguma, revelado a " + "terceiros.\r\n"
					+ "9.2 - As disposições da presente cláusula não se extinguem com o termino "
					+ "ou rescisão deste Contrato, por quaisquer motivos, permanecendo em "
					+ "vigor, a qualquer tempo, as restrições dela decorrentes. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA DECIMA - DA RESCISAO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("10.1 - O presente Contrato poderá, a qualquer tempo, ser rescindido pela "
					+ "LOCADORA ou pelo LOCATARIO, mediante simples notificação escrita, feita "
					+ "com antecedência mínima de 30 (trinta) dias, não gerando rescisão "
					+ "efetuada nos termos desta cláusula, obrigação ou direito, de "
					+ "indenização, reparação ou compensação, seja a que título for devendo ser "
					+ "saldado apenas o valor relativo aos serviços efetivamente prestados e " + "ainda não pagos.\r\n"
					+ "10.2 - Deixando qualquer das partes de cumprir as condições "
					+ "estabelecidas neste Contrato, poderá a parte prejudicada pelo "
					+ "inadimplemento, considerar rescindido o mesmo.\r\n"
					+ "10.3 - O presente Contrato só será rescindido após o acerto final de "
					+ "contas entre as partes. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add(new Phrase("CLAUSULA DECIMA SEGUNDA - DO FORO: ", BOLD_UNDERLINED));
			p.add("\r\n");
			p.add("\r\n");
			p.add("12.1 - Fica eleito o Foro da Comarca de Niquelândia-Goiás, como o único "
					+ "competente para a solução de questões oriundas do presente Contrato que, "
					+ "amigavelmente, as partes não puderem resolver, em prejuízo de qualquer "
					+ "outro por mais privilegiado que seja ou venha a ser, arcando a parte "
					+ "vencida em caso de demanda, com todas as despesas processuais e " + "honorários advocatícios. ");

			p.add("\r\n");
			p.add("\r\n");
			p.add("Niquelândia, DATA: " + alugar.getDtLocacao());

			p.add("\r\n");
			p.add("\r\n");
			p.add("______________________________           _______________________________ ");
			p.add("\r\n");
			p.add("\r\n");
			p.add("           LOCATÁRIO                                               LOCADORA ");

			document.add(titulo);
			document.add(p);

			document.close();

		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		} finally {
			document.close();
		}
		return null;
	}
}
