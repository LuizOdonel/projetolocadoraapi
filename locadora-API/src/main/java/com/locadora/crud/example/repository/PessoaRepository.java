package com.locadora.crud.example.repository;


import com.locadora.crud.example.entity.Pessoa;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {
  Pessoa findByCpf(String paramString);
  
  List<Pessoa> findAllByCpf(String paramString);
}
